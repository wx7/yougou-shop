import { request } from "../../request/index.js";

Page({
  data: {
    swiperList: [],
    categories: [],
    floorList: []
  },
  //options(Object)
  onLoad: function (options) {
    // wx.request({
    //   url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
    //   method: 'GET',
    //   success: (result) => {
    //     console.log(result);
    //     this.setData({
    //       swiperList: result.data.message
    //     });
    //   }
    // });
    this.getSwiper();
    this.getCategories();
    this.getFloorList();
  },
  getSwiper() {
    request({ url: "/home/swiperdata" })
      .then(result => {
        this.setData({
          swiperList: result
        })
      });
  },

  getCategories() {
    request({ url: "/home/catitems" })
      .then(result => {
        this.setData({
          categories: result
        })
      });
  },
  getFloorList() {
    request({ url: "/home/floordata" })
      .then(result => {
        this.setData({
          floorList: result
        })
      });
  },
  onReady: function () {

  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  },
  onPageScroll: function () {

  },
  onTabItemTap: function (item) {

  }
});